var gulp = require('gulp'),

    uglify = require('gulp-uglify'),

    rename = require('gulp-rename');


//compress js code
gulp.task('minifyjs', function() {

	return gulp.src('src/*.js')
			   .pipe(rename({suffix:'.min'}))
			   .pipe(uglify())
			   .pipe(gulp.dest('dist'));
});

gulp.task('default', function() {
	gulp.start('minifyjs');
});

